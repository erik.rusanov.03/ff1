#
FROM python:3.11-slim

#
WORKDIR /code/backend

#
COPY backend/requirements.txt /code/requirements.txt

#
RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt

#
COPY backend /code/backend/

#
COPY frontend /code/frontend

#
CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0","--reload", "--port", "80"]