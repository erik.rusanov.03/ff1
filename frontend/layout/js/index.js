const toggleModal = (open = true) => {
  if (open) document.body.classList.add("modal-open");
  else document.body.classList.remove("modal-open");
};

async function copyToClipboard() {
     var url = window.location.href;

    // Создаем временный элемент для копирования текста
    var tempInput = document.createElement("input");
    tempInput.value = url;
    document.body.appendChild(tempInput);
    tempInput.select();
    tempInput.setSelectionRange(0, 99999);
    document.execCommand("copy");
    document.body.removeChild(tempInput);




  try {
    await navigator.clipboard.writeText(url);
    toggleModal(true);
  } catch (err) {
    console.error("Failed to copy: ", err);
  }

  const closeBtn = document.querySelector(".modal__close");
  closeBtn.addEventListener("click", () => {
    toggleModal(false);
  });
}

const saveInput = (input) => {
  let label = input.nextElementSibling;
  // TODO change value in DB
  console.log(label);
  label.style.opacity = 1;
  setTimeout(() => {
    label.style.opacity = 0;
  }, 1000);
};

const saveProjectName = () => {
  const input = document.getElementById("project-name");
  input.addEventListener("blur", () => saveInput(input));
};

const saveTasksName = () => {
  const inputs = document.querySelectorAll(".task__name");
  inputs.forEach((input) => {
    input.addEventListener("blur", () => saveInput(input));
  });
};

const openCloseTasks = () => {
  const tasks = document.querySelectorAll(".task");

  tasks.forEach((task) => {
    const button = task.querySelector(".task__btn");

    button.addEventListener("click", () => {
      task.classList.toggle("task_opened");
    });
  });
};

document.addEventListener("DOMContentLoaded", () => {
  const copyBtn = document.getElementById("copy-link");
  copyBtn.addEventListener("click", () => copyToClipboard());
  saveProjectName();
  saveTasksName();
  openCloseTasks();
});

document.addEventListener("DOMContentLoaded", () => {
  const filesInput = document.querySelectorAll(".dropzone");
  filesInput.forEach(fileInput => {
  fileInput.addEventListener('change', () => {
            document.querySelector('.dropzone-form').submit();

});
  })
});

const addTask = async (url_hash, user_id) => {
    console.log(user_id);
    try {
        const response = await fetch(`http://localhost:8000/cowora/projects/${url_hash}/add_tasks?` + new URLSearchParams({
    user_id: user_id,
}), {
            method: "POST",
        });
        if (!response.ok) {
            throw new Error("Failed to add task");
        }
        console.log("Task added successfully");
        location.reload();
    } catch (error) {
        console.error("Failed to add task: ", error);
    }
}

const deleteTask = async (url_hash,task_id, user_id) => {
    try {
        const response = await fetch(`http://localhost:8000/cowora/projects/${url_hash}/delete_task/${task_id}?` + new URLSearchParams({
    user_id: user_id,
}), {
            method: "POST",
        });
        if (!response.ok) {
            throw new Error("Failed to delete task");
        }
        console.log("Task deleted successfully");
        location.reload();
    } catch (error) {
        console.error("Failed to delete task: ", error);
    }
}

const takeTask = async (url_hash,task_id, user_id) => {
    try {
        const response = await fetch(`http://localhost:8000/cowora/projects/${url_hash}/update_task_user_id/${task_id}?` + new URLSearchParams({
    user_id: user_id,
}), {
            method: "POST",
        });

        if (!response.ok) {
            throw new Error("Failed to take task");
        }
        console.log("Task took successfully");
        location.reload();
    } catch (error) {
        console.error("Failed to take task: ", error);
    }
}

const clearTask = async (url_hash,task_id, user_id) => {
    try {
        const response = await fetch(`http://localhost:8000/cowora/projects/${url_hash}/clear_task/${task_id}?`  + new URLSearchParams({
    user_id: user_id,
}), {
            method: "POST",
        });
        if (!response.ok) {
            throw new Error("Failed to cleared task");
        }
        console.log("Task cleared successfully");
        location.reload();
    } catch (error) {
        console.error("Failed to cleared task: ", error);
    }
}