from sqlalchemy import create_engine

from app.core.config import settings
from alembic import command, config
from sqlalchemy.orm import sessionmaker

engine = create_engine(str(settings.DB_URL))

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


def run_upgrade():
    with engine.begin() as conn:
        cfg = config.Config("alembic.ini")
        cfg.attributes["connection"] = conn
        command.upgrade(cfg, "head")


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
