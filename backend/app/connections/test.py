from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from app.core.config import settings
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship

from app.models.project_file import ProjectFile
from app.models.projects import Project

engine = create_engine(str(settings.DB_URL))
Session = sessionmaker(bind=engine)

Base = declarative_base()


def add_project_with_files(project_name, file_list):
    session = Session()
    try:
        # Создание нового проекта
        new_project = Project(project_name=project_name, admin_id=2, url_hash="assdssf")

        new_project.files.extend(
            [
                ProjectFile(project=new_project, file_name=file_info['file_name'],
                            path_to_file=file_info['path_to_file'])
                for file_info in file_list
            ]
        )
        session.add(new_project)

        # Сохранение изменений в базу данных
        session.commit()
        print(f"Проект '{project_name}' и его файлы успешно добавлены.")
    except Exception as e:
        session.rollback()
        print(f"Произошла ошибка при добавлении проекта: {e}")
    finally:
        session.close()

# file_list = [
#     {'file_name': 'example1.txt', 'path_to_file': '/path/to/example1.txt'},
#     {'file_name': 'example2.txt', 'path_to_file': '/path/to/example2.txt'}
# ]
#
# add_project_with_files('Новый Проект', file_list)
