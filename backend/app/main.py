from starlette.staticfiles import StaticFiles

from app.core.get_app import get_application

app = get_application()
app.mount("/static", StaticFiles(directory="../frontend/layout"), name="static")
