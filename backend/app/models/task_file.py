from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship

from app.models.base import Base


class TaskFile(Base):
    """
    Таблица с путями до файлов проекта
    """
    task_id = Column(Integer, ForeignKey('task.id'), nullable=False)
    file_name = Column(String(255), nullable=False)
    path_to_file = Column(String(255), nullable=False)
    task = relationship("Task", back_populates="task_files")
