from sqlalchemy import Column, Integer, String, Boolean, ForeignKey
from sqlalchemy.orm import relationship

from app.models.base import Base


class Task(Base):
    """
    Таблица с задачами к каждому проекту
    """
    project_id = Column(Integer, ForeignKey('project.id'), nullable=False)
    user_id = Column(Integer, nullable=True)
    name = Column(String(255), nullable=False)
    description = Column(String(1000), nullable=True)
    ready = Column(Boolean, default=False, nullable=False)

    project = relationship("Project", back_populates="tasks")
    task_files = relationship("TaskFile", back_populates="task")
