from sqlalchemy import Integer, Column
from sqlalchemy.orm import declared_attr, as_declarative


@as_declarative()
class Base:
    """
    Base class for declarative SQLAlchemy models.
    """
    id = Column(Integer, primary_key=True)

    @declared_attr
    def __tablename__(cls) -> str:
        """
        Generate a table name from the class name.
        """
        name = cls.__name__
        for i, smb in enumerate(name[1:]):
            if smb.isupper():
                name = name[:i + 1] + '_' + name[i + 1:]
        return name.lower()
