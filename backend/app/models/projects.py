from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

from app.models.base import Base


class Project(Base):
    admin_id = Column(Integer, nullable=False)
    url_hash = Column(String(64), nullable=False, unique=True)
    project_name = Column(String(255), nullable=False)
    files = relationship("ProjectFile", back_populates="project", cascade="all, delete-orphan")
    tasks = relationship("Task", back_populates="project")

