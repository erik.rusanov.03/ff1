from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship

from app.models.base import Base


class ProjectFile(Base):
    """
    Таблица с путями до файлов проекта
    """
    project_id = Column(Integer, ForeignKey('project.id'), nullable=False)
    file_name = Column(String(255), nullable=False)
    path_to_file = Column(String(255), nullable=False)
    project = relationship("Project", back_populates="files")
