from pydantic import PostgresDsn
from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    DB_URL: PostgresDsn
    BACKEND_CORS_ORIGINS: list[str] = ["*"]
    CREATE_REDIRECT_URL: str = "http://localhost:8000"

    class Config:
        env_file = ".env"


settings = Settings()
