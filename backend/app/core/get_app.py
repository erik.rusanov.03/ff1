from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware

from app.connections.psql import run_upgrade
from app.core.config import settings
from app.routes import routes

# ! DO NOT DELETE IMPORTS
from app.models.base import Base
from app.models.projects import Project
from app.models.project_file import ProjectFile
from app.models.task import Task
from app.models.task_file import TaskFile


def get_application():
    _app = FastAPI()
    _app.add_middleware(
        CORSMiddleware,
        allow_origins=settings.BACKEND_CORS_ORIGINS,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )
    _app.include_router(routes.router)

    @_app.on_event("startup")
    async def startup_event():
        run_upgrade()

    return _app
