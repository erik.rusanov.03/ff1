from datetime import datetime
import hashlib


def generate_hash(admin_id: int):
    input_string = str(admin_id) + str(datetime.now())
    hash = hashlib.md5()
    hash.update(input_string.encode())
    hashed_str = hash.hexdigest()
    return hashed_str
