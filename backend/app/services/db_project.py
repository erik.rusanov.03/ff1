from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from app.core.config import settings
from sqlalchemy.ext.declarative import declarative_base

from app.models.projects import Project

engine = create_engine(str(settings.DB_URL))
Session = sessionmaker(bind=engine)

Base = declarative_base()


def add_project(admin_id, url_hash, project_name):
    try:
        session = Session()
        new_project = Project(
            admin_id=admin_id,
            url_hash=url_hash,
            project_name=project_name
        )

        session.add(new_project)
        session.commit()
        print(f"Проект '{project_name}' и его файлы успешно добавлены.")
    except Exception as e:
        session.rollback()
        print(f"Произошла ошибка при добавлении проекта: {e}")
    finally:
        session.close()


def update_project_name_by_url_hash(admin_id, url_hash, new_project_name):
    session = Session()
    try:
        project = session.query(Project).filter_by(url_hash=url_hash, admin_id=admin_id).first()
        if project:
            project.project_name = new_project_name
            session.commit()
            print(f"Имя проекта успешно обновлено на '{new_project_name}'.")
        else:
            print(f"Проект с url_hash '{url_hash}' не найден или у вас нет прав на его изменение.")
    except Exception as e:
        session.rollback()
        print(f"Произошла ошибка при обновлении проекта: {e}")
    finally:
        session.close()


def list_project_url_hashes_by_admin_id(admin_id):
    session = Session()
    try:
        projects = session.query(Project).filter_by(admin_id=admin_id).all()
        if projects:
            print(f"Проекты пользователя с admin_id {admin_id}")
            return projects
        else:
            print(f"Проекты пользователя с admin_id {admin_id} не найдены.")
    except Exception as e:
        print(f"Произошла ошибка при извлечении данных: {e}")
    finally:
        session.close()


def get_project_id_by_url_hash(url_hash, only_id: bool = True):
    session = Session()
    try:
        # Запрашиваем проект по url_hash
        project = session.query(Project).filter_by(url_hash=url_hash).first()
        if project:
            print(f"ID проекта с url_hash '{url_hash}' равен {project.id}.")
            return project.id if only_id else project
        else:
            print(f"Проект с url_hash '{url_hash}' не найден.")
            return None
    except Exception as e:
        session.rollback()
        print(f"Произошла ошибка при поиске проекта: {e}")
        return None
    finally:
        session.close()


def delete_project_by_url_hash(admin_id, url_hash):
    session = Session()
    try:
        project = session.query(Project).filter_by(url_hash=url_hash, admin_id=admin_id).first()
        if project:
            session.delete(project)
            session.commit()
            print(f"Проект с url_hash '{url_hash}' успешно удален.")
        else:
            print(f"Проект с url_hash '{url_hash}' не найден или у вас нет прав на его удаление.")
    except Exception as e:
        session.rollback()
        print(f"Произошла ошибка при удалении проекта: {e}")
    finally:
        session.close()
