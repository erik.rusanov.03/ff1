from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from app.core.config import settings
from sqlalchemy.ext.declarative import declarative_base

from app.models.project_file import ProjectFile
from app.models.projects import Project
from app.models.task_file import TaskFile

engine = create_engine(str(settings.DB_URL))
Session = sessionmaker(bind=engine)

Base = declarative_base()


def save_file_info_to_db(project_id: int, file_name: str, file_path: str):
    try:
        session = Session()
        db_file = ProjectFile(
            project_id=project_id,
            file_name=file_name,
            path_to_file=file_path
        )
        session.add(db_file)
        session.commit()
        print(f"{file_name} for {project_id} to save to the {file_path}")
        return True
    except Exception as e:
        session.rollback()
        print(f"Failed to save file info to DB: {e}")
        return False
    finally:
        session.close()


def save_task_file_info_to_db(task_id: int, file_name: str, file_path: str):
    try:
        # Создаем новую сессию с помощью контекстного менеджера
        with Session() as session:
            task_file = TaskFile(
                task_id=task_id,
                file_name=file_name,
                path_to_file=file_path
            )
            session.add(task_file)
            session.commit()  # Фиксация изменений в базе данных
            print(f"Task file info saved to DB successfully: {file_name}")
            return True
    except Exception as e:
        # Печать и возвращение ошибки, если commit не удался
        print(f"Failed to save task file info to DB: {e}")
        return False

def get_task_path(project_id):
    session = Session()
    try:
        task_path = session.query(ProjectFile).filter(ProjectFile.project_id == project_id).first()
        if task_path != None:
            return task_path.path_to_file
        else:
            return ""
    except Exception as e:
        session.rollback()
    finally:
        session.close()
