from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, joinedload

from app.core.config import settings
from sqlalchemy.ext.declarative import declarative_base

from app.models.projects import Project
from app.models.task import Task
from app.models.task_file import TaskFile

engine = create_engine(str(settings.DB_URL))
Session = sessionmaker(bind=engine)

Base = declarative_base()


def add_tasks(project_id, description=None, user_id=0):
    session = Session()
    try:
        # Считаем количество задач в этом проекте
        task_count = session.query(Task).filter(Task.project_id == project_id).count()
        # Генерируем имя задачи
        task_name = f"Задача с номером {task_count + 1}"

        new_task = Task(
            project_id=project_id,
            user_id=user_id,
            name=task_name,
            description=description,
            ready=False
        )
        session.add(new_task)
        session.commit()
        print(f"Task '{task_name}' added successfully.")
        return new_task
    except Exception as e:
        session.rollback()
        print(f"Failed to add task: {e}")
    finally:
        session.close()


def delete_task(task_id, project_id):
    session = Session()
    try:
        # Находим задачу по ID и project_id
        task = session.query(Task).filter(Task.id == task_id, Task.project_id == project_id).first()
        if task:
            session.delete(task)
            session.commit()
            print(f"Task with ID {task_id} from project {project_id} deleted successfully.")
            return f"Task with ID {task_id} from project {project_id} deleted successfully."
        else:
            print("Task not found.")
            return "Task not found."
    except Exception as e:
        session.rollback()
        print(f"Failed to delete task: {e}")
        return f"Failed to delete task: {e}"
    finally:
        session.close()


def update_task_name(task_id, project_id, new_name):
    session = Session()
    try:
        # Находим задачу по ID и project_id
        task = session.query(Task).filter(Task.id == task_id, Task.project_id == project_id).first()
        if task:
            task.name = new_name  # Обновляем имя
            session.commit()
            print(f"Name of task with ID {task_id} from project {project_id} updated to '{new_name}'.")
            return f"Name of task with ID {task_id} from project {project_id} updated to '{new_name}'."
        else:
            print("Task not found.")
            return "Task not found."
    except Exception as e:
        session.rollback()
        print(f"Failed to update task name: {e}")
        return f"Failed to update task name: {e}"
    finally:
        session.close()


def update_task_user_id(task_id, project_id, new_user_id):
    session = Session()
    try:
        task = session.query(Task).filter(Task.id == task_id, Task.project_id == project_id).first()
        if task:
            task.user_id = new_user_id  # Обновляем user_id
            session.commit()
            print(f"User ID of task with ID {task_id} updated to '{new_user_id}'.")
            return f"User ID of task with ID {task_id} updated to '{new_user_id}'."
        else:
            print("Task not found.")
            return "Task not found."
    except Exception as e:
        session.rollback()
        print(f"Failed to update user ID: {e}")
        return f"Failed to update user ID: {e}"
    finally:
        session.close()


def update_task_ready(task_id, project_id):
    session = Session()
    try:
        # Поиск задачи по ID и project_id
        task = session.query(Task).filter(Task.id == task_id, Task.project_id == project_id).first()
        if task:
            # Переключаем статус 'ready'
            task.ready = not task.ready
            session.commit()
            status = 'ready' if task.ready else 'not ready'
            print(f"Status of task with ID {task_id} updated to {status}.")
            return f"Status of task with ID {task_id} updated to {status}."
        else:
            print("Task not found.")
            return "Task not found."
    except Exception as e:
        session.rollback()
        print(f"Failed to toggle task status: {e}")
        return f"Failed to toggle task status: {e}"
    finally:
        session.close()


def get_all_tasks(project_id):
    session = Session()
    try:
        tasks = session.query(Task).filter(Task.project_id == project_id).options(joinedload(Task.task_files)).all()
        return tasks
    except Exception as e:
        session.rollback()
    finally:
        session.close()


def clear_task(task_id, project_id):
    session = Session()
    try:
        # Находим задачу по ID и project_id
        task = session.query(Task).filter(Task.id == task_id, Task.project_id == project_id).first()
        task_file = session.query(TaskFile).filter(TaskFile.task_id == task_id).all()
        if task and task_file:
            task.ready = False
            for file in task_file:
                print(f"DELETE THIS FILE {file}")
                session.delete(file)
            session.commit()
            print(f"Task with ID {task_id} from project {project_id} clear successfully.")
            return f"Task with ID {task_id} from project {project_id} clear successfully."
        else:
            print("Task not found.")
            return "Task not found."
    except Exception as e:
        session.rollback()
        print(f"Failed to clear task: {e}")
        return f"Failed to clear task: {e}"
    finally:
        session.close()
