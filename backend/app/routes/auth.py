from auth_lib.fastapi import UnionAuth
from fastapi import FastAPI, status, Depends
from fastapi.routing import APIRouter
from fastapi.responses import RedirectResponse
import asyncio
import aiohttp

router = APIRouter(
    prefix="/login",
    tags=["Auth"]
)


@router.get("/")
async def login(me=Depends(UnionAuth(scopes=[], allow_none=False, auto_error=True))):
    print(me)
    pass


@router.get("/auth")
async def get_user_info(me=Depends(UnionAuth(scopes=[], allow_none=False, auto_error=True))):
    print(me)
    pass
