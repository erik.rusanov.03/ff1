from fastapi.routing import APIRouter
from app.routes import auth, tasks, base

router = APIRouter(
    prefix="/cowora",
    tags=["Main"]
)

router.include_router(auth.router)
router.include_router(base.router)
router.include_router(tasks.router)
