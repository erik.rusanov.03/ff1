import os
import shutil

from fastapi import Request, APIRouter, Depends, Query, Path, HTTPException, UploadFile, File, status, Form
from fastapi.responses import RedirectResponse, FileResponse
from starlette.responses import JSONResponse
from starlette.status import HTTP_200_OK, HTTP_500_INTERNAL_SERVER_ERROR
from fastapi.templating import Jinja2Templates
from auth_lib.fastapi import UnionAuth

from app.core.config import settings
from app.services.db_file import save_file_info_to_db, save_task_file_info_to_db, get_task_path
from app.services.hash_processing import generate_hash
from app.services.db_project import add_project, update_project_name_by_url_hash, list_project_url_hashes_by_admin_id, \
    delete_project_by_url_hash, get_project_id_by_url_hash
from app.services.db_task import add_tasks, delete_task, update_task_name, update_task_user_id, update_task_ready, \
    get_all_tasks, clear_task

router = APIRouter(
    prefix="/projects",
    tags=["Auth"]
)

templates = Jinja2Templates(directory="../frontend/layout")


@router.post("/create")
async def create_project(
        me_id=Depends(UnionAuth(
            scopes=[],
            allow_none=False,
            auto_error=True,
        )),
        project_name: str = Query(
            "Ботаем что-то",
            min_length=1,
            max_length=255,
            description="The name of the project",
        )
):
    admin_id = me_id['id']
    url_hash = generate_hash(admin_id=admin_id)
    add_project(admin_id=admin_id, url_hash=url_hash, project_name=project_name)

    return {
        "redirect_url": f"{settings.CREATE_REDIRECT_URL.rstrip()}/cowora/projects/{url_hash}?user_id={admin_id}"
    }


@router.post("/{url_hash}/update")
async def update_project(url_hash: str,
                         me_id=Depends(UnionAuth(scopes=[], allow_none=False, auto_error=True)),
                         new_project_name: str = Query(..., min_length=1, max_length=255,
                                                       description="The name of the project"),
                         ):
    admin_id = me_id['id']
    update_project_name_by_url_hash(admin_id=admin_id, url_hash=url_hash, new_project_name=new_project_name)
    return {"admin_id": admin_id, "url_hash": url_hash, "new_project_name": new_project_name}


@router.post("/{url_hash}/delete")
async def delete_project(url_hash: str,
                         me_id=Depends(UnionAuth(scopes=[], allow_none=False, auto_error=True)),
                         ):
    admin_id = me_id['id']
    delete_project_by_url_hash(admin_id=admin_id, url_hash=url_hash)
    return {"message": "Project deleted", "admin_id": admin_id, "url_hash": url_hash}


@router.get("/all_project")
async def all_project(
        me_id=Depends(UnionAuth(scopes=[], allow_none=False, auto_error=True))):
    admin_id = me_id['id']
    all_project = list_project_url_hashes_by_admin_id(admin_id=admin_id)
    return {"admin_id": admin_id, "all_project": all_project}


@router.get("/{url_hash}")
async def project(
        url_hash: str,
        # me_id=Depends(UnionAuth(scopes=[], allow_none=False, auto_error=True)),
        request: Request,
        user_id: int,
        token: str = None
):
    _project = get_project_id_by_url_hash(url_hash, False)
    project_id = _project.id
    tasks = get_all_tasks(project_id)
    task_path = get_task_path(project_id)

    try:
        return templates.TemplateResponse(
            "index.html",
            {
                "request": request,
                "tasks": tasks,
                "url_hash": url_hash,
                "task_path": task_path,
                "project_id": project_id,
                "user_id": user_id,
                "project_name": _project.project_name
            }
        )
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


@router.post("/{url_hash}/clear_task/{task_id}")
async def clear_tasks(
        url_hash: str,
        task_id: int,
        user_id: int,
        # me_id=Depends(UnionAuth(scopes=[], allow_none=False, auto_error=True)),
        request: Request):
    project_id = get_project_id_by_url_hash(url_hash)
    clear_task(task_id, project_id)
    response = RedirectResponse(url=f"/cowora/projects/{url_hash}?user_id={user_id}",
                                status_code=status.HTTP_303_SEE_OTHER)
    return response


@router.post("/{url_hash}/add_tasks")
async def add_task(
        url_hash: str,
        user_id: int,
        # me_id=Depends(UnionAuth(scopes=[], allow_none=False, auto_error=True)),
        request: Request):
    print(url_hash)
    project_id = get_project_id_by_url_hash(url_hash)
    print(project_id)
    for _ in range(4):
        add_tasks(project_id)

    response = RedirectResponse(url=f"/cowora/projects/{url_hash}?user_id={user_id}",
                                status_code=status.HTTP_303_SEE_OTHER)
    return response


@router.post("/{url_hash}/delete_task/{task_id}")
async def delete_tasks(
        url_hash: str,
        task_id: int,
        user_id: int,
        # me_id=Depends(UnionAuth(scopes=[], allow_none=False, auto_error=True)),
        request: Request):
    project_id = get_project_id_by_url_hash(url_hash)
    delete_task(task_id, project_id)
    response = RedirectResponse(url=f"/cowora/projects/{url_hash}?user_id={user_id}",
                                status_code=status.HTTP_303_SEE_OTHER)
    return response


@router.get("/{url_hash}/update_task_name/{task_id}")
async def update_task_names(
        url_hash: str,
        task_id: int,
        request: Request,
        new_name: str = Query(..., description="The new name for the task")):
    project_id = get_project_id_by_url_hash(url_hash)
    update_task_name(task_id, project_id, new_name)
    return templates.TemplateResponse("index.html", {"request": request})


@router.post("/{url_hash}/update_task_user_id/{task_id}")
async def update_task_user_ids(
        url_hash: str,
        task_id: int,
        request: Request,
        user_id: int,
        token: str = None,
):
    """
    Начать работу
    """
    project_id = get_project_id_by_url_hash(url_hash)
    update_task_user_id(task_id, project_id, user_id)
    return templates.TemplateResponse("index.html", {"request": request})


@router.get("/{url_hash}/update_task_ready/{task_id}")
async def update_task_readys(
        url_hash: str,
        task_id: int,
        request: Request):
    """
    Задача выполнена/невыполнена
    """
    project_id = get_project_id_by_url_hash(url_hash)
    update_task_ready(task_id, project_id)
    return templates.TemplateResponse("index.html", {"request": request})


@router.post("/{url_hash}/upload")
async def upload_file(
        request: Request,
        url_hash: str,
        user_id: int,
        file: UploadFile = File(...)):
    # Путь, где будут сохраняться файлы
    project_id = get_project_id_by_url_hash(url_hash)
    upload_folder = f"uploaded_files/{project_id}"
    os.makedirs(upload_folder, exist_ok=True)

    try:
        # Сохраняем файл
        file_path = os.path.join(upload_folder, file.filename)
        with open(file_path, "wb") as buffer:
            shutil.copyfileobj(file.file, buffer)
        file.file.close()

        if save_file_info_to_db(project_id, file.filename, file_path):
            response = RedirectResponse(url=f"/cowora/projects/{url_hash}?user_id={user_id}",
                                        status_code=status.HTTP_303_SEE_OTHER)
            return response
        else:
            raise HTTPException(status_code=HTTP_500_INTERNAL_SERVER_ERROR,
                                detail="Failed to save file info to the database")

    except Exception as e:
        raise HTTPException(status_code=HTTP_500_INTERNAL_SERVER_ERROR, detail=str(e))


@router.post("/{url_hash}/upload_task/{task_id}")
async def upload_task_file(
        url_hash: str,
        user_id: int,
        task_id: int = Path(..., description="The ID of the task"),
        file: UploadFile = File(...)):
    # Путь, где будут сохраняться файлы
    project_id = get_project_id_by_url_hash(url_hash)
    upload_folder = f"uploaded_files/{project_id}/tasks"
    os.makedirs(upload_folder, exist_ok=True)

    try:
        # Сохраняем файл
        file_path = os.path.join(upload_folder, file.filename)
        with open(file_path, "wb") as buffer:
            shutil.copyfileobj(file.file, buffer)
        file.file.close()

        if save_task_file_info_to_db(task_id, file.filename, file_path):
            update_task_ready(task_id, project_id)
            response = RedirectResponse(
                url=f"/cowora/projects/{url_hash}?user_id={user_id}",
                status_code=status.HTTP_303_SEE_OTHER
            )
            return response
        else:
            raise HTTPException(status_code=HTTP_500_INTERNAL_SERVER_ERROR,
                                detail="Failed to save file info to the database")

    except Exception as e:
        raise HTTPException(status_code=HTTP_500_INTERNAL_SERVER_ERROR, detail=str(e))


@router.get("/uploaded_files/{file_path:path}")
async def get_file(file_path):
    orig_path = os.path.join(
        os.path.abspath(os.path.dirname(__file__)),
        f"../../uploaded_files/{file_path}")
    return FileResponse(orig_path)
