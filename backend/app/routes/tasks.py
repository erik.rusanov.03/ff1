from fastapi import FastAPI, status, Depends, UploadFile, File
from fastapi.routing import APIRouter
from fastapi.responses import RedirectResponse
from fastapi.templating import Jinja2Templates
import os

templates = Jinja2Templates(directory="frontend")

router = APIRouter(
)


@router.get("/{proj_id}")
async def get_tasks(proj_id: int):
    pass


@router.post("/add_task")
async def add_task(hash_project: str):
    # TODO
    return 0


@router.post("/show_task/{task_id}")
async def get_task_info(task_id: int):
    # TODO get info about task from DB
    return templates.TemplateResponse("index.html")


@router.post("/take_task/{task_id}")
async def take_task(task_id: int):
    # TODO get task from DB and change userID

    return templates.TemplateResponse("index.html")


@router.post("/delete/{task_id}")
async def delete_task(task_id: int):
    # TODO полностью удаляет задачу
    pass


@router.post("/clear/{task_id}")
async def clear_task(task_id: int):
    # TODO очищает все, связанное с таском, но не удаляет таск
    pass


@router.post("/load_tasks")
async def add_tasks(tasks: UploadFile = File(...)):
    try:
        with open(tasks.filename, "wb") as f:
            f.write(await tasks.read())
    except Exception as e:
        return RedirectResponse(url=f'{router.url_path_for("home")}',
                                status_code=status.HTTP_303_SEE_OTHER)


@router.get("/load_solve/")
async def add_solve(solve: UploadFile = File(...)):
    try:
        with open(solve.filename, "wb") as f:
            f.write(await solve.read())
    except Exception as e:
        return RedirectResponse(url=f'{router.url_path_for("home")}',
                                status_code=status.HTTP_303_SEE_OTHER)
    return 0
